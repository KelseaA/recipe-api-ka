# Kelsea Anderson - Frontend Skill Test

## Accomplished

- Pull the data from the API
- Create a list view which includes all the recipes
- Create a recipe detail view to display each recipe
- Ingredients with a matching `ingredientId` listed in the specials response should also show the special `title`, `type` and `text` under the ingredient name

### Uses
- HTML5
- CSS3
- JavaScript
- Vue.js
- Gulp

To install and run, use the commands below:

- `npm i`
- `npm run start`
