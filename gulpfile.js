'use strict';

var argv = require('yargs').argv;
var autoprefixer = require('autoprefixer');
var gulp = require('gulp');
var rename = require('gulp-rename');
var named = require('vinyl-named');
var plumber = require('gulp-plumber');
var postcss = require('gulp-postcss');
var sass = require('gulp-sass');
var uglify = require('gulp-uglify');
var watch = require('gulp-watch');
var webpack = require('webpack');
var gulpWebpack = require('gulp-webpack');
var del = require('del');
var runSequence = require('run-sequence');

// Load configurations & set variables
var config = {
    port: 3000,

    sass: {
        outputStyle: "compressed",
    },

    autoprefixer: {
        browsers: [
            "> 1%",
            "last 2 versions",
            "Firefox ESR",
        ]
    },

    js: {
        entry: [
            "main.js",
        ]
    },
};
var entry = [];

/**
 * Set default & build tasks
 */
for (var i = 0; i <= config.js.entry.length - 1; i++) {
    entry.push('public/js/' + config.js.entry[i]);
}

gulp.task('sass', function() {
    return gulp.src('public/css/main.scss')
        .pipe(sass())
        .pipe(sass({ outputStyle: config.sass.outputStyle }).on('error', sass.logError))
        .pipe(postcss([
            autoprefixer({
                browsers: config.autoprefixer.browsers
            })
        ]))
        .pipe(gulp.dest('public/css/dist'));
});

/**
 * Webpack
 *
 * Bundle JavaScript files
 */
gulp.task('webpack', function () {
    return gulp.src(entry)
        .pipe(plumber())
        .pipe(named())
        .pipe(gulpWebpack({
            watch: argv.watch ? true : false,
            module: {
                loaders: [
                    {
                        test: /\.js$/,
                        exclude: /node_modules/,
                        loader: 'babel-loader',
                        query: {
                            presets: ['es2015']
                        }
                    },
                    {
                        test: /\.(png|jpg|gif|svg)$/,
                        loader: 'file-loader',
                        options: {
                            name: '[name].[ext]?[hash]'
                        }
                    }
                ]
            },
        }))
        .pipe(uglify())
        .pipe(rename({ suffix: '.min' }))
        .pipe(gulp.dest('public/js/dist'));
});

gulp.task('webpack-dev', function () {
    return gulp.src(entry)
        .pipe(plumber())
        .pipe(named())
        .pipe(gulpWebpack({
            watch: argv.watch ? true : false,
            devtool: 'source-map',
            module: {
                loaders: [
                    {
                        test: /\.js$/,
                        exclude: /node_modules/,
                        loader: 'babel-loader',
                        query: {
                            presets: ['es2015']
                        }
                    },
                    {
                        test: /\.(png|jpg|gif|svg)$/,
                        loader: 'file-loader',
                        options: {
                            name: '[name].[ext]?[hash]'
                        }
                    }
                ]
            },
        }))
        .pipe(gulp.dest('public/js/dist'));
});

// For internal use only
gulp.task('_webpack', function () {
    argv.watch = true;
    gulp.start('webpack');
    gulp.start('webpack-dev');
});

/**
 * Default task, running just `gulp` will compile the sass, js, and watch files.
 */
gulp.task('default', [], function () {
    gulp.start('_webpack');

    watch(['public/css/**/*.scss', '!public/css/dist'], function () {
        gulp.start('sass');
    });
});

gulp.task('clean:dist', function() {
    return del.sync('dist');
});

gulp.task('build', function (callback) {
    runSequence('clean:dist', 
      ['webpack', 'sass'],
      callback
    )
});