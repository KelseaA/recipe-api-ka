import Vue from 'vue'
import App from './app.vue'
import Menu from './menu.js';

new Vue({
    el: '#app',
    render: h => h(App)
});

menu();

function menu() {
    window.Menu = new Menu();
}