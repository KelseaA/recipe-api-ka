export default class Menu {
    constructor() {
        this.app = document.querySelector(".js-app");
        this.hamburgerMenu = document.querySelector(".js-hamburger");
        this.menuLinks = document.querySelectorAll('.js-menu-item');

        this.addListeners();
    }

    addListeners() {   
        var self = this;

        this.hamburgerMenu.addEventListener("click", function(){
            self.app.classList.toggle("menu-open");
        });

        document.addEventListener("click", function (event) {
            if (event.target.classList.contains("js-menu-item")) {
                if (self.app.classList.contains("menu-open")) {
                    self.app.classList.remove("menu-open");
                    window.scrollTo(0, 0);
                }
            }
        }, false);
    }
}